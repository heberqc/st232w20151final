package UNI_FIIS.Connectivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionBD {

    private static Connection cn = null;

    public static Connection getConnection(Proxy proxy) {
        try {
            String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
            
            if (cn == null) {
                Class.forName(driver).newInstance();
                cn = DriverManager.getConnection(proxy.getConnectionString());
            }
        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException e) {
            //throw new RuntimeException("Base de datos no existe.");
            System.out.println("ERROR (ConexionBD): " + e.toString());
        }
        return cn;
    }

    public static void closeConnection() {
        try {
            if (cn != null) {
                cn.close();
            }
        } catch (SQLException ex) {
            System.out.println("No se pudo cerra la conexión: " + ex.toString());
        }
    }
}
