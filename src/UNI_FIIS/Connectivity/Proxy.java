package UNI_FIIS.Connectivity;

public class Proxy {
    private String server;
    private String database;
    private String user;
    private String password;
    
    public Proxy(){
        server = "localhost\\SQLExpress";
        database = "AcademicDB";
        user = "sa";
        password = "password_sa";
    }
  
    public String getConnectionString(){
        String connectionUrl = "jdbc:sqlserver://"+this.server+":1433;databaseName="+this.database+";user="+this.user+";password="+this.password;
        return connectionUrl;
    }    
    
}
