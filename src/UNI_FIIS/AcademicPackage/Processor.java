package UNI_FIIS.AcademicPackage;

import java.util.ArrayList;

public class Processor {

    ScoreContainer calculateFinalScore(ScoreContainer sc, String evs) {
        int pPP = 1;
        int pEP = 1;
        int pEF = 1;
        switch (evs) {
            case "F":
                pEF = 2;
                break;
            case "H":
                pEF = 2;
                pPP = 2;
                break;
            case "I":
                pPP = 2;
                break;
        }
        sc.PP = (sc.PC1 + sc.PC2 + sc.PC3 + sc.PC4 + sc.PC5 - Math.min(Math.min(Math.min(sc.PC1, sc.PC2), Math.min(sc.PC3, sc.PC4)), sc.PC5)) / 4;
        sc.finalScore = (sc.PP * pPP + sc.EP * pEP + sc.EF * pEF) / (pPP + pEP + pEF);
        return sc;
    }

    float getWeightedAverage(ArrayList<Course> aCourses) {
        float weightedAverage = 0;
        int totCred = 0;
        int canCred = 0;
        for (int i = 0; i < aCourses.size(); i++) {
            canCred = aCourses.get(i).getCredits();
            totCred = totCred + canCred;
            weightedAverage = weightedAverage + canCred * aCourses.get(i).getScores().finalScore;
        }
        weightedAverage = weightedAverage / ((1.0F) * totCred);
        return weightedAverage;
    }
}
