package UNI_FIIS.AcademicPackage;

import UNI_FIIS.Connectivity.ConexionBD;
import UNI_FIIS.Connectivity.Proxy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Retriever {

    public int academicYear;
    public int semester;
    public int studentID;

    public ArrayList<Course> retrieveCourses() {
        ArrayList<Course> alCourses = new ArrayList<>();
        // Consultar a la BD para obtener los cursos y colocarlos en el ArrayList (alCourses)
        try {
            Connection c = ConexionBD.getConnection(new Proxy());
            PreparedStatement ps = c.prepareStatement("SELECT DISTINCT [CourseID] FROM [Scores] WHERE [AcademicYear] = ? AND [Semester] = ? AND [StudentID] = ?;");
            ps.setInt(1, academicYear);
            ps.setInt(2, semester);
            ps.setInt(3, studentID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                alCourses.add(new Course(academicYear, semester, studentID, rs.getInt(1)));
            }
        } catch (Exception ex) {
            System.out.println("ERROR: " + ex.toString());
        }
        return alCourses;
    }

    public ScoreContainer retrieveCourseScores(int studentID, int courseID) {
        ScoreContainer sc = new ScoreContainer();
        // Llenar sc con las notas obtenidas desde la BD
        try {
            Connection c = ConexionBD.getConnection(new Proxy());
            PreparedStatement ps = c.prepareStatement("SELECT * FROM [Scores] WHERE [AcademicYear] = ? AND [Semester] = ? AND [StudentID] = ? AND [CourseID] = ?;");
            ps.setInt(1, academicYear);
            ps.setInt(2, semester);
            ps.setInt(3, studentID);
            ps.setInt(4, courseID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getInt(4) == courseID) {
                    sc.PC1 = rs.getFloat(5);
                    sc.PC2 = rs.getFloat(6);
                    sc.PC3 = rs.getFloat(7);
                    sc.PC4 = rs.getFloat(8);
                    sc.PC5 = rs.getFloat(9);
                    sc.EP = rs.getFloat(11);
                    sc.EF = rs.getFloat(12);
                    sc.ES = rs.getFloat(13);
                    break;
                }
            }
        } catch (Exception ex) {
            System.out.println("ERROR: " + ex.toString());
        }
        return sc;
    }
}
