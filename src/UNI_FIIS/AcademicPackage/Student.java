package UNI_FIIS.AcademicPackage;

import UNI_FIIS.Connectivity.ConexionBD;
import UNI_FIIS.Connectivity.Proxy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Student {

    private int ID;
    private String name;
    public ArrayList<Course> courses;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        try {
            Connection c = ConexionBD.getConnection(new Proxy());
            PreparedStatement ps = c.prepareStatement("select * from [Student] where [StudentID] = ?;");
            ps.setInt(1, ID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getInt(1) == ID) {
                    name = rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4);
                    break;
                }
            }
        } catch (Exception ex) {
            System.out.println("ERROR: " + ex.toString());
            name = "(ERROR)";
        }
        return name;
    }

    public void retrieveCourses(int AcademicYear, int Semester) {
        Retriever r = new Retriever();
        r.studentID = ID;
        r.semester = Semester;
        r.academicYear = AcademicYear;
        courses = r.retrieveCourses();
    }

    public float getWeightedAverage() {
        float f = 0.0F;
        /// Calcular el promedio ponderado
        f = new Processor().getWeightedAverage(courses);
        return f;
    }

}
