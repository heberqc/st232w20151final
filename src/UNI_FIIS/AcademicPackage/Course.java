package UNI_FIIS.AcademicPackage;

import UNI_FIIS.Connectivity.ConexionBD;
import UNI_FIIS.Connectivity.Proxy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Course {

    private int ID;
    private String name;
    private int credits;
    private String evaluationSystem;
    private String section;
    private ScoreContainer scores;

    public Course(int AcademicYear, int Semester, int studentID, int courseID) {
        // Inicializar scores con las notas delcurso obtenidas desde la BD
        // usando retrieveCourseData (name, credits, evaluationSystem, section) y retrieveScores (scores)
        ID = courseID;
        retrieveCourseData();
        scores = retrieveScores(AcademicYear, Semester, studentID, courseID);
    }

    private void retrieveCourseData() {
        // Obtener desde la BD los datos del curso: name, credits, evaluationSystem, section
        try {
            Connection c = ConexionBD.getConnection(new Proxy());
            PreparedStatement ps = c.prepareStatement("SELECT * FROM [Course] WHERE [CourseID] = ?;");
            ps.setInt(1, ID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getInt(1) == ID) {
                    section = rs.getString(3);
                    name = rs.getString(4);
                    credits = rs.getInt(5);
                    evaluationSystem = rs.getString(6);
                    break;
                }
            }
        } catch (Exception ex) {
            System.out.println("ERROR: " + ex.toString());
        }
    }

    private ScoreContainer retrieveScores(int AcademicYear, int Semester, int studentID, int courseID) {
        ScoreContainer sc = new ScoreContainer();
        Retriever r = new Retriever();
        r.academicYear = AcademicYear;
        r.semester = Semester;
        r.studentID = studentID;
        // Utiliza un objeto Retriever para obtener las notas del curso 
        sc = new Processor().calculateFinalScore(r.retrieveCourseScores(studentID, courseID), evaluationSystem);
        return sc;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public int getCredits() {
        return credits;
    }

    public String getEvaluationSystem() {
        return evaluationSystem;
    }

    public String getSection() {
        return section;
    }

    public ScoreContainer getScores() {
        return scores;
    }

}
