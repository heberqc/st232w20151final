
import UNI_FIIS.AcademicPackage.Course;
import UNI_FIIS.AcademicPackage.Student;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class JavaFX_JDBC_EXFINAL extends Application {

    @Override
    public void start(Stage primaryStage) {

        Text txtTitle = new Text("CONSULTA DE NOTAS POR ALUMNO");

        /*-------------------hbAlumno-------------------*/
        Text txtAnio = new Text("Año: ");
        Text txtSemest = new Text("Semestre: ");
        Text txtCodAlu = new Text("ID. Alumno: ");
        Text txtCodCur = new Text("ID. Curso: ");

        VBox vbtxtEntry = new VBox(15);
        vbtxtEntry.getChildren().addAll(txtAnio, txtSemest, txtCodAlu, txtCodCur);

        TextField txtFldAnio = new TextField();
        txtFldAnio.setMaxWidth(60);
        TextField txtFldSemest = new TextField();
        txtFldSemest.setMaxWidth(30);

        TextField txtFldIDAlu = new TextField();
        txtFldIDAlu.setMaxWidth(30);

        TextField txtFldIDCur = new TextField();
        txtFldIDCur.setMaxWidth(30);

        VBox vbtxtFldEntry = new VBox(5);
        vbtxtFldEntry.getChildren().addAll(txtFldAnio, txtFldSemest, txtFldIDAlu, txtFldIDCur);

        Image imageSearch = new Image(getClass().getResourceAsStream("search.png"));
        Button btnSearch = new Button();
        btnSearch.setGraphic(new ImageView(imageSearch));

        VBox vbbtnSearch = new VBox(5);
        vbbtnSearch.setAlignment(Pos.CENTER);
        vbbtnSearch.setPadding(new Insets(15, 15, 15, 15));
        vbbtnSearch.getChildren().add(btnSearch);

        HBox hbEntry = new HBox(5);
        hbEntry.setPadding(new Insets(15, 15, 15, 15));
        hbEntry.getChildren().addAll(vbtxtEntry, vbtxtFldEntry, vbbtnSearch);
        /*----------------------------------------------------*/

        /**/
        Text txtTitleResult = new Text("RESULTADOS: ");
        HBox hbrTitleResult = new HBox(5);
        hbrTitleResult.getChildren().add(txtTitleResult);
        /**/
        /*---------------hbResult----------------------*/
        Text txtAlumno = new Text("Alumno: ");
        Text txtCurso = new Text("Curso: ");
        Text txtNota = new Text("Nota del Curso: ");
        Text txtPromPonderado = new Text("Prom.Ponderado: ");
        VBox vbtxtResult = new VBox(15);
        vbtxtResult.getChildren().addAll(txtAlumno, txtCurso, txtNota, txtPromPonderado);

        // Datos de resultado "en duro" a modo de ejemplo. 
        // Estos deberían obtenerse utilizando objetos y clases que consultan a la base de datos
        Text txtAluFullName = new Text();
        Text txtCursoName = new Text();
        Text txtOutputNota = new Text();
        Text txtOutputPromPonderado = new Text();
        //--------------------------------------------------------------------------------------

        VBox vbtxtResultData = new VBox(15);
        vbtxtResultData.getChildren().addAll(txtAluFullName, txtCursoName, txtOutputNota, txtOutputPromPonderado);

        HBox hbResult = new HBox(5);
        hbResult.setPadding(new Insets(15, 15, 15, 15));
        hbResult.getChildren().addAll(vbtxtResult, vbtxtResultData);

        /*----------------------------------------------------*/
        btnSearch.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
        // Los datos de Año, Semestre, ID de Alumno e ID de curso son criterios de busqueda
                // Instanciar las clases necesarias e invocar los métodos que sean necesarios para obtener 
                // el resultado y mostrarlo en la sección "RESULTADOS:" de la pantalla 
                int studentID = Integer.parseInt(txtFldIDAlu.getText());
                txtAluFullName.setText("");
                txtCursoName.setText("");
                txtOutputNota.setText("");
                txtOutputPromPonderado.setText("");
                Student student = new Student();
                student.setID(studentID);
                Course course = new Course(Integer.parseInt(txtFldAnio.getText()), Integer.parseInt(txtFldSemest.getText()), studentID, Integer.parseInt(txtFldIDCur.getText()));
                txtAluFullName.setText(student.getName());
                txtCursoName.setText(course.getName());
                txtOutputNota.setText(String.valueOf(course.getScores().finalScore));
                student.retrieveCourses(Integer.parseInt(txtFldAnio.getText()), Integer.parseInt(txtFldSemest.getText()));
                txtOutputPromPonderado.setText(String.valueOf(student.getWeightedAverage()));
//                System.out.println("Buscar en base a : Año>"+txtFldAnio.getText()+" Mes>"+txtFldSemest.getText()+" ID Alumno>"+txtFldIDAlu.getText()+" ID Curso>"+txtFldIDCur.getText());
            }
        });

        /*----------------------------------------------------*/
        VBox paneV = new VBox(5);
        paneV.setAlignment(Pos.CENTER);
        paneV.setPadding(new Insets(15, 15, 15, 15));
        paneV.getChildren().addAll(txtTitle, hbEntry, hbrTitleResult, hbResult);

        StackPane canvas = new StackPane();
        canvas.getChildren().add(paneV);

        Scene display = new Scene(canvas);
        primaryStage.setScene(display);
        primaryStage.setTitle("UNI-FIIS");
        primaryStage.setResizable(false);
        primaryStage.show();

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
